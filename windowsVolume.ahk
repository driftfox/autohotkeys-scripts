; WINDOWS VOLUME CONTROL
; Custom volume buttons
^!NumpadAdd:: Send {Volume_Up} ;shift + numpad plus
^!NumpadSub:: Send {Volume_Down} ;shift + numpad minus
^!NumpadEnter::Send {Volume_Mute} ; Break key mutes
; This last one will allow mute while * is held down
^!NumpadMult::
Send {Volume_Mute}
loop
{
   if not getkeystate("NumpadMult", "p")
    {
      send, {Volume_Mute}
      break
    }
}
return