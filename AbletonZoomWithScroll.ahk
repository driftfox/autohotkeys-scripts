#NoEnv                       ; Recommended for performance and compatibility with future AutoHotkey releases.
#KeyHistory 0                ; Disable key history
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#Persistent                  ; Keep script running in the background

; Only allow in Ableton
#IfWinActive, ahk_exe Ableton Live 9 Suite.exe

; HotKeys to capture
!WheelUp::
Send {NumpadAdd}
Return

!WheelDown::
Send {NumpadSub}
Return

#IfWinActive

; How to debug this script if it does not work:
; https://autohotkey.com/boards/viewtopic.php?t=10252 